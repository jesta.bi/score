<script>
$(function(){
	$('#ot11').hide();
	$('#ot22').hide();
	$('#ot33').hide();
	$('#ot44').hide();
	
	$('input[id=ot01]').click(function(){
		var test = $('input[id=ot01]:checked').val();
		if(typeof(test) == "undefined"){
			$('#ot11').hide();
		}else{
			$('#ot11').fadeIn();
		}
	});	
	$('input[id=ot02]').click(function(){
		var test = $('input[id=ot02]:checked').val();
		if(typeof(test) == "undefined"){
			$('#ot22').hide();
		}else{
			$('#ot22').fadeIn();
		}
	});	
	$('input[id=ot03]').click(function(){
		var test = $('input[id=ot03]:checked').val();
		if(typeof(test) == "undefined"){
			$('#ot33').hide();
		}else{
			$('#ot33').fadeIn();
		}
	});	
	$('input[id=ot04]').click(function(){
		var test = $('input[id=ot04]:checked').val();
		if(typeof(test) == "undefined"){
			$('#ot44').hide();
		}else{
			$('#ot44').fadeIn();
		}
	});	
	
	
});
</script>
<script>
function check_tag(){
	var q1 = $('input[name=q1]:checked').val();
    if(typeof(q1) == "undefined"){ // 檢查完全沒有選取
               alert("請填寫問題一");
               return false;
        }
	var q2 = $('input[name=q2]:checked').val();
    if(typeof(q2) == "undefined"){ // 檢查完全沒有選取
               alert("請填寫問題二");
               return false;
        }
	var q3 = $('input[name=q3]:checked').val();
    if(typeof(q3) == "undefined"){ // 檢查完全沒有選取
               alert("請填寫問題三");
               return false;
        }
	var q4 = $('input[name=q4]:checked').val();
    if(typeof(q4) == "undefined"){ // 檢查完全沒有選取
               alert("請填寫問題四");
               return false;
        }	
	var q5 = $('input[name=q5]:checked').val();
    if(typeof(q5) == "undefined"){ // 檢查完全沒有選取
               alert("請填寫問題五");
               return false;
        }
	var q6 = $('input[name=q6]:checked').val();
    if(typeof(q6) == "undefined"){ // 檢查完全沒有選取
               alert("請填寫問題六");
               return false;
        }
	var q7 = $('input[name=q7]:checked').val();
    if(typeof(q7) == "undefined"){ // 檢查完全沒有選取
               alert("請填寫問題七");
               return false;
        }
	var q8 = $('input[name=q8]:checked').val();
    if(typeof(q8) == "undefined"){ // 檢查完全沒有選取
               alert("請填寫問題八");
               return false;
        }
	var q9 = $('input[name=q9]:checked').val();
    if(typeof(q9) == "undefined"){ // 檢查完全沒有選取
               alert("請填寫問題九");
               return false;
        }	
	var q10 = $('input[name=q10]:checked').val();
    if(typeof(q10) == "undefined"){ // 檢查完全沒有選取
               alert("請填寫問題十");
               return false;
        }
	var q11 = $('input[name=q11]:checked').val();
    if(typeof(q11) == "undefined"){ // 檢查完全沒有選取
               alert("請填寫問題十一");
               return false;
        }
	var q12 = $('input[name=q12]:checked').val();
    if(typeof(q12) == "undefined"){ // 檢查完全沒有選取
               alert("請填寫問題十二");
               return false;
        }	
	var q13 = $('input[name=q13]:checked').val();
    if(typeof(q13) == "undefined"){ // 檢查完全沒有選取
               alert("請填寫問題十三");
               return false;
        }	
		
		
		
    
	
	
	
}

    window.history.forward();
    function noBack() { window.history.forward(); }

</script>

<H1><?php echo $class_row_result["ClassName"]; ?></H1>
<H2><?php echo $course_row_result["name"]; ?></H2>
<h2><font color="#0000CC">工業局意見調查表</font></h2>

<?php
  
	  if($_COOKIE[$sn] == $classid.$courseid){
		echo "<div class='score_block' style='height:auto;'><p><font><a href='check.php'>已進行過該課程評點<br></a></font></p>";
        exit();
	  }
?>

<form action="Db_recIND.php" method="post" onSubmit="return check_tag(this);" id="rec"  name="rec">
<table width="100%" class="scoreTable">
<tr>
<td width="50%"></td>
<td width="10%"><b>非常滿意</b></td>
<td width="10%"><b>滿意</b></td>
<td width="10%"><b>普通</b></td>
<td width="10%"><b>不滿意</b></td>
<td width="10%"><b>非常不滿意</b></td>
</tr>

<tr><td colspan="11" align="left"><font size="+2" color="#0000CC"><b>[課程]</b></font></td></tr>

<tr>
<td align="left">01.課程目標之明確性</td>
<td><input type="radio" name="q1" value="5"></td>
<td><input type="radio" name="q1" value="4"></td>
<td><input type="radio" name="q1" value="3"></td>
<td><input type="radio" name="q1" value="2"></td>
<td><input type="radio" name="q1" value="1"></td>
</tr>

<tr>
<td align="left">02.課程難易度</td>
<td><input type="radio" name="q2" value="5"></td>
<td><input type="radio" name="q2" value="4"></td>
<td><input type="radio" name="q2" value="3"></td>
<td><input type="radio" name="q2" value="2"></td>
<td><input type="radio" name="q2" value="1"></td>
</tr>

<tr>
<td align="left">03.課程時數安排</td>
<td><input type="radio" name="q3" value="5"></td>
<td><input type="radio" name="q3" value="4"></td>
<td><input type="radio" name="q3" value="3"></td>
<td><input type="radio" name="q3" value="2"></td>
<td><input type="radio" name="q3" value="1"></td>
</tr>

<tr>
<td align="left">04.教材內容</td>
<td><input type="radio" name="q4" value="5"></td>
<td><input type="radio" name="q4" value="4"></td>
<td><input type="radio" name="q4" value="3"></td>
<td><input type="radio" name="q4" value="2"></td>
<td><input type="radio" name="q4" value="1"></td>
</tr>

<tr>
<td align="left">05.課程實用度</td>
<td><input type="radio" name="q5" value="5"></td>
<td><input type="radio" name="q5" value="4"></td>
<td><input type="radio" name="q5" value="3"></td>
<td><input type="radio" name="q5" value="2"></td>
<td><input type="radio" name="q5" value="1"></td>
</tr>

<tr>
<td align="left">06.課程評量方式</td>
<td><input type="radio" name="q6" value="5"></td>
<td><input type="radio" name="q6" value="4"></td>
<td><input type="radio" name="q6" value="3"></td>
<td><input type="radio" name="q6" value="2"></td>
<td><input type="radio" name="q6" value="1"></td>
</tr>


<tr><td colspan="11" align="left"><font size="+2" color="#0000CC"><b>[講師]</b></font></td></tr>

<tr>
<td align="left">07.具備之專業知識</td>
<td><input type="radio" name="q7" value="5"></td>
<td><input type="radio" name="q7" value="4"></td>
<td><input type="radio" name="q7" value="3"></td>
<td><input type="radio" name="q7" value="2"></td>
<td><input type="radio" name="q7" value="1"></td>
</tr>

<tr>
<td align="left">08.表達能力</td>
<td><input type="radio" name="q8" value="5"></td>
<td><input type="radio" name="q8" value="4"></td>
<td><input type="radio" name="q8" value="3"></td>
<td><input type="radio" name="q8" value="2"></td>
<td><input type="radio" name="q8" value="1"></td>
</tr>

<tr>
<td align="left">09.教學方式對於協助你解決問題的幫助程度</td>
<td><input type="radio" name="q9" value="5"></td>
<td><input type="radio" name="q9" value="4"></td>
<td><input type="radio" name="q9" value="3"></td>
<td><input type="radio" name="q9" value="2"></td>
<td><input type="radio" name="q9" value="1"></td>
</tr>

<tr><td colspan="11" align="left"><font size="+2" color="#0000CC"><b>[課程學習環境]</b></font></td></tr>
<tr>
<td align="left">10.教學環境及實作設備</td>
<td><input type="radio" name="q10" value="5"></td>
<td><input type="radio" name="q10" value="4"></td>
<td><input type="radio" name="q10" value="3"></td>
<td><input type="radio" name="q10" value="2"></td>
<td><input type="radio" name="q10" value="1"></td>
</tr>
<tr>
<td align="left">11.執行單位提供之各項行政事務事項</td>
<td><input type="radio" name="q11" value="5"></td>
<td><input type="radio" name="q11" value="4"></td>
<td><input type="radio" name="q11" value="3"></td>
<td><input type="radio" name="q11" value="2"></td>
<td><input type="radio" name="q11" value="1"></td>
</tr>

<tr><td colspan="11" align="left"><font size="+2" color="#0000CC"><b>[綜合意見]</b></font></td></tr>

<tr>
<td align="left">12.課程整體評價</td>
<td><input type="radio" name="q12" value="5"></td>
<td><input type="radio" name="q12" value="4"></td>
<td><input type="radio" name="q12" value="3"></td>
<td><input type="radio" name="q12" value="2"></td>
<td><input type="radio" name="q12" value="1"></td>
</tr>
<tr>
<td align="left">13.提升工作能力之幫助程度</td>
<td><input type="radio" name="q13" value="5"></td>
<td><input type="radio" name="q13" value="4"></td>
<td><input type="radio" name="q13" value="3"></td>
<td><input type="radio" name="q13" value="2"></td>
<td><input type="radio" name="q13" value="1"></td>
</tr>


<?php ////////////////////////////////?>
<table class="scoreTable4" width="100%" style="margin-top:20px;">
<tr>
<td align="left"  colspan="4"><font size="+2" color="#0000CC"><b>[意見填寫] 請先從以下選項挑選分類</b></font></td>
</tr>

<!--------------意見欄位分類--------------------->

<tr>

<td width="12%" align="left"><input type="checkbox" name="other_1" value="1" onClick="ot_1" id="ot01">&nbsp;課程</td>
<td width="12%" align="left"><input type="checkbox" name="other_2" value="1" onClick="ot_2" id="ot02">&nbsp;學習環境</td>
<td width="12%" align="left"><input type="checkbox" name="other_3" value="1" onClick="ot_3" id="ot03">&nbsp;講師</td>
<td width="64%" align="left"><input type="checkbox" name="other_4" value="1" onClick="ot_4" id="ot04">&nbsp;綜合</td>


</tr>


<!--------------意見欄位分類--------------------->


<tr id="ot11">
<td width="10%">[課程]</td>
<td align="left" colspan="4"><textarea name="other1" value="other1" style=" height:50px; width:100%;"  resize="false"></textarea></td>
</tr>

<tr id="ot22">
<td width="10%">[學習環境]</td>
<td align="left" colspan="4"><textarea name="other2" value="other2" style=" height:50px; width:100%;"  resize="false"></textarea></td>
</tr>

<tr id="ot33">
<td width="10%">[講師]</td>
<td align="left" colspan="4"><textarea name="other3" value="other3" style=" height:50px; width:100%;"  resize="false"></textarea></td>
</tr>

<tr id="ot44">
<td width="10%">[綜合]</td>
<td align="left" colspan="4"><textarea name="other4" value="other4" style=" height:50px; width:100%;"  resize="false"></textarea></td>
</tr>






<tr >
<td align="left" colspan="3">如果您希望中心回應意見請留下姓名 :
</td>
</tr>
<tr >
<td align="left" colspan="3"><textarea name="note" value="note" style=" height:30px; width:150px;"  resize="false"></textarea>      
</td>
</tr>

<tr>
<input type="hidden" name="sn" value="<?php echo $sn;?>" >
<input type="hidden" name="classid" value="<?php echo $classid;?>" >
<input type="hidden" name="courseid" value="<?php echo $courseid;?>" >
<td height="40px" align="center"><input type="submit" value="送出評點"/></td>
</tr>
</table>

<?php ////////////////////////////////?>

</form>