<script>
$(function(){
	$('#ot11').hide();
	$('#ot22').hide();
	$('#ot33').hide();
	$('#ot44').hide();
	$('#ot55').hide();
	$('input[id=ot01]').click(function(){
		var test = $('input[id=ot01]:checked').val();
		if(typeof(test) == "undefined"){
			$('#ot11').hide();
		}else{
			$('#ot11').fadeIn();
		}
	});	
	$('input[id=ot02]').click(function(){
		var test = $('input[id=ot02]:checked').val();
		if(typeof(test) == "undefined"){
			$('#ot22').hide();
		}else{
			$('#ot22').fadeIn();
		}
	});	
	$('input[id=ot03]').click(function(){
		var test = $('input[id=ot03]:checked').val();
		if(typeof(test) == "undefined"){
			$('#ot33').hide();
		}else{
			$('#ot33').fadeIn();
		}
	});	
	$('input[id=ot04]').click(function(){
		var test = $('input[id=ot04]:checked').val();
		if(typeof(test) == "undefined"){
			$('#ot44').hide();
		}else{
			$('#ot44').fadeIn();
		}
	});	
	$('input[id=ot05]').click(function(){
		var test = $('input[id=ot05]:checked').val();
		if(typeof(test) == "undefined"){
			$('#ot55').hide();
		}else{
			$('#ot55').fadeIn();
		}
	});	
	
});
</script>
<script>
function check_tag(){
	var q1 = $('input[name=q1]:checked').val();
    if(typeof(q1) == "undefined"){ // 檢查完全沒有選取
               alert("請填寫問題一");
               return false;
        }
	var q2 = $('input[name=q2]:checked').val();
    if(typeof(q2) == "undefined"){ // 檢查完全沒有選取
               alert("請填寫問題二");
               return false;
        }
	var q3 = $('input[name=q3]:checked').val();
    if(typeof(q3) == "undefined"){ // 檢查完全沒有選取
               alert("請填寫問題三");
               return false;
        }
	var q4 = $('input[name=q4]:checked').val();
    if(typeof(q4) == "undefined"){ // 檢查完全沒有選取
               alert("請填寫問題四");
               return false;
        }	
	var q5 = $('input[name=q5]:checked').val();
    if(typeof(q5) == "undefined"){ // 檢查完全沒有選取
               alert("請填寫問題五");
               return false;
        }
	var q6 = $('input[name=q6]:checked').val();
    if(typeof(q6) == "undefined"){ // 檢查完全沒有選取
               alert("請填寫問題六");
               return false;
        }
}
window.history.forward();
    function noBack() { window.history.forward(); }
</script>
<H1><?php echo $class_row_result["ClassName"]; ?></H1>
<H2><?php echo $course_row_result["name"]; ?></H2>
<h2><font color="#0000CC">班級課程意見調查表REC019</font></h2>

<?php
  
	  if($_COOKIE[$sn] == $classid.$courseid){
		echo "<div class='score_block' style='height:auto;'><p><font><a href='check.php'>已進行過該課程評點<br></a></font></p>";
        exit();
	  }
?>

<form action="Db_rec019.php" method="post" onSubmit="return check_tag(this);" id="rec"  name="rec">

<table width="100%" class="scoreTable">
<tr>
<td width="50%"></td>
<td width="5%"><b>很好</b></td>
<td width="5%"></td>
<td width="5%"></td>
<td width="5%"></td>
<td width="5%"></td>
<td width="5%"></td>
<td width="5%"></td>
<td width="5%"></td>
<td width="5%"></td>
<td width="5%"><b>很不好</b></td>
</tr>

<tr><td colspan="11" align="left"><font size="+2" color="#0000CC"><b>[課程]</b></font></td></tr>
<tr>
<td align="left">1.您對於本課程內容</td>
<td>9&nbsp;<input type="radio" name="q1" value="9"></td>
<td>8&nbsp;<input type="radio" name="q1" value="8"></td>
<td>7&nbsp;<input type="radio" name="q1" value="7"></td>
<td>6&nbsp;<input type="radio" name="q1" value="6"></td>
<td>5&nbsp;<input type="radio" name="q1" value="5"></td>
<td>4&nbsp;<input type="radio" name="q1" value="4"></td>
<td>3&nbsp;<input type="radio" name="q1" value="3"></td>
<td>2&nbsp;<input type="radio" name="q1" value="2"></td>
<td>1&nbsp;<input type="radio" name="q1" value="1"></td>
<td>0&nbsp;<input type="radio" name="q1" value="-1"></td>
</tr>

<tr><td colspan="11" align="left"><font size="+2" color="#0000CC"><b>[設備]</b></font><font color="#0099CC"></font></td></tr>
<tr>
<td align="left">2.您對於本課程之硬體設備支援上機實作</td>
<td>9&nbsp;<input type="radio" name="q2" value="9"></td>
<td>8&nbsp;<input type="radio" name="q2" value="8"></td>
<td>7&nbsp;<input type="radio" name="q2" value="7"></td>
<td>6&nbsp;<input type="radio" name="q2" value="6"></td>
<td>5&nbsp;<input type="radio" name="q2" value="5"></td>
<td>4&nbsp;<input type="radio" name="q2" value="4"></td>
<td>3&nbsp;<input type="radio" name="q2" value="3"></td>
<td>2&nbsp;<input type="radio" name="q2" value="2"></td>
<td>1&nbsp;<input type="radio" name="q2" value="1"></td>
<td>0&nbsp;<input type="radio" name="q2" value="-1"></td>
</tr>

<tr>
<td align="left">3.您對於本課程之軟體環境配合課程需求</td>
<td>9&nbsp;<input type="radio" name="q3" value="9"></td>
<td>8&nbsp;<input type="radio" name="q3" value="8"></td>
<td>7&nbsp;<input type="radio" name="q3" value="7"></td>
<td>6&nbsp;<input type="radio" name="q3" value="6"></td>
<td>5&nbsp;<input type="radio" name="q3" value="5"></td>
<td>4&nbsp;<input type="radio" name="q3" value="4"></td>
<td>3&nbsp;<input type="radio" name="q3" value="3"></td>
<td>2&nbsp;<input type="radio" name="q3" value="2"></td>
<td>1&nbsp;<input type="radio" name="q3" value="1"></td>
<td>0&nbsp;<input type="radio" name="q3" value="-1"></td>
</tr>

<tr><td colspan="11" align="left"><font size="+2" color="#0000CC"><b>[講師]</b></font></td></tr>
<tr>
<td align="left">4.您對於講師的教學方式</td>
<td>9&nbsp;<input type="radio" name="q4" value="9"></td>
<td>8&nbsp;<input type="radio" name="q4" value="8"></td>
<td>7&nbsp;<input type="radio" name="q4" value="7"></td>
<td>6&nbsp;<input type="radio" name="q4" value="6"></td>
<td>5&nbsp;<input type="radio" name="q4" value="5"></td>
<td>4&nbsp;<input type="radio" name="q4" value="4"></td>
<td>3&nbsp;<input type="radio" name="q4" value="3"></td>
<td>2&nbsp;<input type="radio" name="q4" value="2"></td>
<td>1&nbsp;<input type="radio" name="q4" value="1"></td>
<td>0&nbsp;<input type="radio" name="q4" value="-1"></td>
</tr>

<tr>
<td align="left">5.您對於講師在此課程領域之專業知識</td>
<td>9&nbsp;<input type="radio" name="q5" value="9"></td>
<td>8&nbsp;<input type="radio" name="q5" value="8"></td>
<td>7&nbsp;<input type="radio" name="q5" value="7"></td>
<td>6&nbsp;<input type="radio" name="q5" value="6"></td>
<td>5&nbsp;<input type="radio" name="q5" value="5"></td>
<td>4&nbsp;<input type="radio" name="q5" value="4"></td>
<td>3&nbsp;<input type="radio" name="q5" value="3"></td>
<td>2&nbsp;<input type="radio" name="q5" value="2"></td>
<td>1&nbsp;<input type="radio" name="q5" value="1"></td>
<td>0&nbsp;<input type="radio" name="q5" value="-1"></td>
</tr>

<tr><td colspan="11" align="left"><font size="+2" color="#0000CC"><b>[教材]</b></font></td></tr>
<tr>
<td align="left">6.您認為教材內容能符合課程講授之需要</td>
<td>9&nbsp;<input type="radio" name="q6" value="9"></td>
<td>8&nbsp;<input type="radio" name="q6" value="8"></td>
<td>7&nbsp;<input type="radio" name="q6" value="7"></td>
<td>6&nbsp;<input type="radio" name="q6" value="6"></td>
<td>5&nbsp;<input type="radio" name="q6" value="5"></td>
<td>4&nbsp;<input type="radio" name="q6" value="4"></td>
<td>3&nbsp;<input type="radio" name="q6" value="3"></td>
<td>2&nbsp;<input type="radio" name="q6" value="2"></td>
<td>1&nbsp;<input type="radio" name="q6" value="1"></td>
<td>0&nbsp;<input type="radio" name="q6" value="-1"></td>
</tr>

</table>

<table class="scoreTable4" width="100%" style="margin-top:20px;">
<tr>
<td align="left"  colspan="4"><font size="+2" color="#0000CC"><b>[意見填寫] 請先從以下選項挑選分類</b></font></td>
</tr>

<!--------------意見欄位分類--------------------->

<tr>

<td width="12%" align="left"><input type="checkbox" name="other_1" value="1" onClick="ot_1" id="ot01">&nbsp;課程</td>
<td width="12%" align="left"><input type="checkbox" name="other_2" value="1" onClick="ot_2" id="ot02">&nbsp;設備</td>
<td width="12%" align="left"><input type="checkbox" name="other_3" value="1" onClick="ot_3" id="ot03">&nbsp;講師</td>
<td width="12%" align="left"><input type="checkbox" name="other_4" value="1" onClick="ot_4" id="ot04">&nbsp;教材</td>
<td width="52%" align="left"><input type="checkbox" name="other_5" value="1" onClick="ot_5" id="ot05">&nbsp;其他</td>

</tr>


<!--------------意見欄位分類--------------------->


<tr id="ot11">
<td width="10%">[課程]</td>
<td align="left" colspan="5"><textarea name="other1" value="other1" style=" height:50px; width:100%;"  resize="false"></textarea></td>
</tr>

<tr id="ot22">
<td width="10%">[設備]</td>
<td align="left" colspan="5"><textarea name="other2" value="other2" style=" height:50px; width:100%;"  resize="false"></textarea></td>
</tr>

<tr id="ot33">
<td width="10%">[講師]</td>
<td align="left" colspan="5"><textarea name="other3" value="other3" style=" height:50px; width:100%;"  resize="false"></textarea></td>
</tr>

<tr id="ot44">
<td width="10%">[教材]</td>
<td align="left" colspan="5"><textarea name="other4" value="other4" style=" height:50px; width:100%;"  resize="false"></textarea></td>
</tr>

<tr id="ot55">
<td width="10%">[其他]</td>
<td align="left" colspan="5"><textarea name="other5" value="other5" style=" height:50px; width:100%;"  resize="false"></textarea></td>
</tr>




<tr >
<td align="left" colspan="4">如果您希望中心回應意見請留下姓名 :
</td>
</tr>
<tr >
<td align="left" colspan="4"><textarea name="note" value="note" style=" height:30px; width:150px;"  resize="false"></textarea>      
</td>
</tr>

<tr>
<input type="hidden" name="sn" value="<?php echo $sn;?>" >
<input type="hidden" name="classid" value="<?php echo $classid;?>" >
<input type="hidden" name="courseid" value="<?php echo $courseid;?>" >
<td height="40px" align="center"><input type="submit" value="送出評點"/></td>
</tr>
</table>


</form>